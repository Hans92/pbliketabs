//
//  main.m
//  PBLikeTabs
//
//  Created by Kuba Rejnhard on 16.03.2014.
//  Copyright (c) 2014 Whittle by Little. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  TabView.m
//  MobileID
//
//  Created by Kuba Rejnhard on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import "TabView.h"

@implementation TabView
@synthesize color, tabLabel, tabButton, tabBackground, contentView;
- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)frameColor
{
    self = [super initWithFrame:frame];
    if (self) {
        color = frameColor;
        
        self.clicked = NO;
        self.reversed = NO;
        
        tabLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 35, 200, 20)];
        tabLabel.textColor = [UIColor whiteColor];
        
        
        tabButton = [UIButton buttonWithType:UIButtonTypeCustom];
        tabButton.frame = CGRectMake(self.bounds.size.width-38, 25, 38, 42);
        UIImage *tbImage = [UIImage imageNamed:@"reload"];
        tabButton.alpha = 0.0;
        [tabButton setImage:tbImage forState:UIControlStateNormal];
        [tabButton addTarget:self
                      action:@selector(flipView)
            forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:tabButton];
        
        
        [self addSubview:tabLabel];
        
        [self setUserInteractionEnabled:YES];
        
        
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x+20, frame.origin.y+40, frame.size.width, frame.size.height-40)];
        
        [self addSubview:self.contentView];
        
        
        
        
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    
    CGRect smallFrame = CGRectMake(20, 20, self.frame.size.width-40, self.frame.size.height-20);
    
    
    UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:smallFrame  cornerRadius: 8];
    self.layer.cornerRadius = 8; // if you like rounded corners
    self.layer.shadowOffset = CGSizeMake(0, -5);
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.5;
    
    self.layer.shadowPath = roundedRectanglePath.CGPath;
    [color setFill];
    [roundedRectanglePath fill];
    
    
    
    
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touched: %d", self.tabIndex);
    
    
}
-(void)flipView
{
    NSLog(@"Flip: %d", self.tabIndex);
    
    
}
@end

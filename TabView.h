//
//  TabView.h
//  MobileID
//
//  Created by Kuba Rejnhard on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabView : UIView
@property (nonatomic, strong)UIColor *color;
@property int tabIndex;
@property BOOL clicked;
@property BOOL reversed;
@property (nonatomic, strong)UIImage *tabBackground;

- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)frameColor;
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
@property (nonatomic, strong)IBOutlet UILabel *tabLabel;
@property (nonatomic, strong)UIButton* tabButton;
@property (strong, nonatomic) UIView* contentView;
@end

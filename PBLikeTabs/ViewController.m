//
//  ViewController.m
//  MobileID
//
//  Created by Kuba Rejnhard on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import "ViewController.h"

#import "TabView.h"

@interface ViewController ()
@property (nonatomic, strong) NSMutableArray * tabArray;

@end

@implementation ViewController
@synthesize tabArray;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backg"]];
    
 
    NSArray* tabColors = [NSArray arrayWithObjects:
                          [UIColor colorWithRed: 66/255.0f green: 205/255.0f blue: 0/255.0f alpha: 1],
                          [UIColor colorWithRed: 0/255.0f green: 122/255.0f blue: 255/255.0f alpha: 1],
                          [UIColor colorWithRed: 255/255.0f green: 161/255.0f blue: 0/255.0f alpha: 1],
                          [UIColor colorWithRed: 86/255.0f green: 183/255.0f blue: 241/255.0f alpha: 1],
                          [UIColor colorWithRed: 255/255.0f green: 45/255.0f blue: 85/255.0f alpha: 1],
                          [UIColor colorWithRed: 255/255.0f green: 204/255.0f blue: 0/255.0f alpha: 1],
                          [UIColor colorWithRed: 66/255.0f green: 205/255.0f blue: 0/255.0f alpha: 1],
                          [UIColor colorWithRed: 0/255.0f green: 122/255.0f blue: 255/255.0f alpha: 1],
                          [UIColor colorWithRed: 255/255.0f green: 161/255.0f blue: 0/255.0f alpha: 1],
                          nil];
    
    NSArray* tabLabels = [NSArray arrayWithObjects:
                          @"Label 1",
                          @"Label 2",
                          @"Label 3",
                          @"Label 4",
                          @"Label 5",
                          @"Label 6",
                          @"Label 7",
                          @"Label 8",
                          @"Label 9",
                          nil];
    
    
    
    
    
    TabView *tb = [[TabView alloc]init];
    tabArray = [[NSMutableArray alloc]init];
    
    
    for (int i = 0; i<8; i++) {
        tb = [[TabView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+40+i*45, self.view.frame.size.width, self.view.frame.size.height-45) andColor: [tabColors objectAtIndex:i]];
        tb.backgroundColor = [UIColor clearColor];
        tb.alpha = 1.0;
        
        tb.tabIndex = i;
        
        
        tb.tabLabel.text = [tabLabels objectAtIndex:i];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [tb addGestureRecognizer:tapRecognizer];
        
        
        [tabArray addObject:tb];
        [self.view addSubview:tb];
        
    }
    
    
}




- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)tap:(UITapGestureRecognizer*)tapRecognizer{
    
    TabView *myTabView = (TabView*)tapRecognizer.view;
    
    NSLog(@"fire %i", myTabView.tabIndex);
    
    if (myTabView.clicked ==NO){
        [UIView animateWithDuration:0.33f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             if(myTabView.tabIndex == 0){
                                 myTabView.frame = CGRectMake(-15, 40, self.view.bounds.size.width+30, self.view.bounds.size.height-60);
                                 
                             }
                             else{
                                 myTabView.frame = CGRectMake(-15, 40, self.view.bounds.size.width+30, self.view.bounds.size.height-60);
                             }
                             
                             
                             
                             for (int i=myTabView.tabIndex+1; i<tabArray.count;i++){
                                 
                                 ((TabView*)[tabArray objectAtIndex:i]).frame = CGRectMake(0, self.view.bounds.size.height, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.width, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.height);
                                 
                             }
                         }
                         completion:^(BOOL finished){
                             myTabView.clicked = YES;
                             
                             
                             
                             [UIView animateWithDuration:0.33f
                                                   delay:0.0f
                                                 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                                              animations:^{
                                                  myTabView.tabButton.alpha = 1.0;
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  myTabView.clicked = YES;
                                                 
                                                  
                                              }];
                             
                         }];
    }
    
    if (myTabView.clicked ==YES){
        [UIView animateWithDuration:0.1f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             myTabView.tabButton.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             myTabView.clicked = NO;
                             
                             
                             [UIView animateWithDuration:0.33f
                                                   delay:0.0f
                                                 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                                              animations:^{
                                                  myTabView.frame = CGRectMake(0, myTabView.tabIndex*45+40, self.view.bounds.size.width, myTabView.bounds.size.height);
                                                  
                                                  
                                                  for (int i=myTabView.tabIndex+1; i<tabArray.count;i++){
                                                      
                                                      ((TabView*)[tabArray objectAtIndex:i]).frame = CGRectMake(0, ((TabView*)[tabArray objectAtIndex:i]).tabIndex*45+40, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.width, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.height);
                                                  }
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  
                                              }];
                         }];
        
        
    }
}

@end
